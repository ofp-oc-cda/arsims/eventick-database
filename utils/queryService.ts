import {QueryTypes, Transaction} from "sequelize"
import {getSequelize} from "../config"

export const genericUpdate = async (table, id, args, transaction?: Transaction) => {
    // Setup static beginning of query
    const query = [`UPDATE ${table}`];
    query.push("SET")
    console.log("[QueryString]", args)
    // Create another array storing each set command
    // and assigning a number value for parameterized query
    const set: string[] = [];
    const colValues: any[] = [];
    let index = 1;
    Object.keys(args).forEach(function (key) {
        if (typeof args[key] !== "undefined") {
            set.push(key + " = ($" + index + ")")
            index += 1;
            colValues.push(args[key]);
        }
    });
    set.push("updated_at = NOW()");
    query.push(set.join(", "));
    // Add the WHERE statement to look up by id
    query.push(`WHERE id= $${index} RETURNING *`);
    // Return a complete query string
    const queryString = query.join(" ");

    colValues.push(id);

    console.log(queryString);
    console.log(colValues);
    const result = await getSequelize().query(queryString, {
        bind: colValues,
        raw: true,
        plain: true,
        type: QueryTypes.UPDATE,
        transaction: transaction,
    });
    if (result[1] > 0) {
        return result[0];
    } else {
        return null
    }
}
