import {Sequelize, SequelizeOptions} from "sequelize-typescript";

import {Comments, Events, PaymentMethods, Profile, Tickets, Users} from "./models"

const createDatabaseConfig = (): SequelizeOptions => {
    return {
        username: "eventick",
        password: "********",
        database: "eventick",
        host: "event-database.ca3ckydw4f4x.eu-west-3.rds.amazonaws.com",
        dialect: "postgres",
        logQueryParameters: true,
        pool: {
            max: 3,
            min: 0,
            idle: 0,
            acquire: 3000,
            evict: 3000,
        },
        dialectOptions: {
            multipleStatements: true
        },
        query: {
            raw: true
        },
        models: [
            Comments,
            Users,
            PaymentMethods,
            Profile,
            Events,
            Tickets,
        ]
    }
}

let sequelize: Sequelize | null = null;

export const getSequelize = (): Sequelize => {
    if (sequelize === null) {
        throw "'Sequelize is not initialized. Please run 'loadSequelize()'";
    }
    return sequelize;
};

export const loadSequelize = async (): Promise<void> => {
    if (sequelize) {
        sequelize.connectionManager.initPools();
        // restore `getConnection()` if it has been overwritten by `close()`
        // eslint-disable-next-line no-prototype-builtins
        if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            delete await sequelize.connectionManager.getConnection();
        }
    } else {
        console.log("No sequelize instance, creating one");
        const config = createDatabaseConfig();
        if (!config.database || !config.username) {
            throw "Missing credentials.";
        }
        sequelize = new Sequelize(config.database, config.username, config.password, config);
        await sequelize.authenticate();
    }
};

export const closeConnection = async (): Promise<void> => {
    if (sequelize) {
        await sequelize.connectionManager.close();
    }
}
