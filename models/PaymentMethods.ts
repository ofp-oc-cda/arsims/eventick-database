import {DataTypes} from 'sequelize';
import {AllowNull, Column, CreatedAt, Default, Model, Table, Unique, UpdatedAt} from "sequelize-typescript";

@Table({
    underscored: true,
    tableName: 'payment_methods',
})
export default class PaymentMethods extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    declare id: number

    @Default(DataTypes.UUIDV4)
    @AllowNull(false)
    @Column(DataTypes.UUID)
    uuid: string

    @Unique
    @AllowNull(false)
    @Column(new DataTypes.STRING(255))
    card_token: string

    @AllowNull(false)
    @Column(DataTypes.NUMBER)
    user_id: number

    @Default(null)
    @Column(new DataTypes.STRING(255))
    brand: string | null

    @Default(null)
    @Column(new DataTypes.STRING(2))
    country: string | null

    @Column(DataTypes.NUMBER)
    exp_month: number | null

    @Column(DataTypes.NUMBER)
    exp_year: number | null

    @Column(DataTypes.NUMBER)
    last_4: number | null

    @Column(new DataTypes.STRING(100))
    status: string | null

    @CreatedAt
    created_at: Date
    @UpdatedAt
    updated_at: Date
}
