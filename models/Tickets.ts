import {Column, Model, Table} from 'sequelize-typescript'
import {DataTypes} from "sequelize";

@Table({
    underscored: true,
    tableName: 'tickets'
})

export default class Tickets extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true
    })
    declare id: number

    @Column(DataTypes.STRING(255))
    image: string
    @Column(DataTypes.STRING(255))

    title: string
    @Column(DataTypes.STRING(255))
    category: string[]

    @Column(DataTypes.STRING(255))
    description: string
    @Column(DataTypes.FLOAT)
    price: number
}
