import {DataTypes} from 'sequelize'
import {AllowNull, Column, CreatedAt, Default, Model, Table, Unique, UpdatedAt} from 'sequelize-typescript'

@Table({
    underscored: true,
    tableName: 'comments'
})

export default class Comments extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true
    })
    declare id: number

    @Column(DataTypes.TEXT)
    description: string

    @Column(new DataTypes.STRING(100))
    name: string;
    @Column(new DataTypes.STRING(300))
    email: string
}
