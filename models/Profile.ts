import {DataTypes} from 'sequelize'
import {Column, Model, Table} from 'sequelize-typescript'

@Table({
    underscored: true,
    tableName: 'profile'
})

export default class Profile extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true
    })
    declare id: number

    @Column(new DataTypes.STRING(100))
    firstname: string

    @Column(new DataTypes.STRING(100))
    lastname: string

    @Column(new DataTypes.STRING(255))
    profileImage: string

    @Column(DataTypes.STRING(255))
    phone: string;

    @Column(DataTypes.DATE(255))
    dateOfBirth: Date

    @Column(DataTypes.STRING(255))
    gender: string

    @Column(DataTypes.DATE(255))
    joined: Date

    @Column(DataTypes.STRING(255))
    email: string

    @Column(DataTypes.STRING(255))
    address: string
}
