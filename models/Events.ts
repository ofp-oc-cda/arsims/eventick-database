import {DataTypes} from 'sequelize'
import {AllowNull, Column, CreatedAt, Default, Model, Table, Unique, UpdatedAt} from 'sequelize-typescript'

@Table({
    underscored: true,
    tableName: 'events'
})

export default class Events extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true
    })
    declare id: number

    @Column(DataTypes.STRING(255))
    image: string;

    @Column(DataTypes.STRING(255))
    title: string

    @Column(DataTypes.STRING(255))
    category: string[]

    @Column(DataTypes.STRING(255))
    description: string

    @Column(DataTypes.DOUBLE)
    price: number
}
