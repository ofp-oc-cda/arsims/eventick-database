import {DataTypes} from 'sequelize'
import {AllowNull, Column, CreatedAt, Default, Model, Table, Unique, UpdatedAt} from 'sequelize-typescript'

@Table({
    underscored: true,
    tableName: 'users'
})

export default class Users extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true
    })
    declare id: number

    @Default(DataTypes.UUIDV4)
    @AllowNull(false)
    @Column(DataTypes.UUID)
    uuid: string

    @Column(new DataTypes.STRING(100))
    firstname: string

    @Column(new DataTypes.STRING(100))
    lastname: string

    @Unique
    @AllowNull(false)
    @Column(new DataTypes.STRING(15))
    phone: string

    @Default(null)
    @Column(new DataTypes.STRING(255))
    email: string | null

    @Column(DataTypes.TEXT)
    password: string | null

    @Unique
    @Default(null)
    @Column(new DataTypes.STRING(255))
    stripe_customer: string | null

    @CreatedAt
    created_at: Date

    @UpdatedAt
    updated_at: Date
}
