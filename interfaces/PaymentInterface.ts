export interface PaymentInterface {
    id: number,
    uuid: string,
    card_token: string,
    user_id: string,
    brand: string | null,
    country: string | null,
    exp_month: string | null,
    exp_year: string | null,
    last_4: string | null,
    status: string | null,
    created_at: Date | null,
    updated_at: Date,
}
