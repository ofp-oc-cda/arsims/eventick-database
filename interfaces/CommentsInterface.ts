export interface CommentsInterface {
    id: number,
    description: string,
    name: string,
    email: string
}
