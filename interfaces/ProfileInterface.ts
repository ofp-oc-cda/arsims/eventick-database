export interface ProfileInterface {
    id: number,
    firstname: string,
    lastname: string,
     profileImage: string,
    phone: string,
    dateOfBirth: Date;
    gender: string,
    joined: Date
    email: string,
    address: string
}
